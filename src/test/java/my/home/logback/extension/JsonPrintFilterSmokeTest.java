package my.home.logback.extension;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sample.bean.book.Book;

public class JsonPrintFilterSmokeTest {
	private static final Logger standardLogger = LoggerFactory.getLogger("logger1");
	private static final Logger jsonLogger = LoggerFactory.getLogger("logger2");

	/** example of a complex object with nested objects, object tree */
	private Book book;
	
	@Before
	public void before() {
		book = sample.bean.book.SampleInstances.getSampleBook();
	}
	
	@Test
	public void compareVanillaAndJsonOutputs() {
		standardLogger.info("Book {}", book);
		jsonLogger.info("Book {}", book);
	}
}
