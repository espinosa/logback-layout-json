package my.home.logback.extension;

/**
 * Simples possible, test implementation of {@link JsonPrintPatternLayoutConfig}.
 * All permissible, using default implelemtantion of JSON serializer.
 * 
 * @author Espinosa
 */
public class TestConfig implements JsonPrintPatternLayoutConfig {

	@Override
	public JsonSerializer createSerializer() {
		return new JacksonSerializer();
	}

	@Override
	public boolean isAllowed(Object objectToBeSerialized) {
		return true;
	}
}
