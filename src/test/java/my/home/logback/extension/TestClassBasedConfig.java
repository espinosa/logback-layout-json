package my.home.logback.extension;

import java.util.HashSet;
import java.util.Set;

import sample.bean.book.Book;
import sample.bean.book.Section;

public class TestClassBasedConfig extends ClassBasedConfig {

	@Override
	public Set<Class<?>> allowedClasses() {
		 HashSet<Class<?>> r = new HashSet<>();
		 r.add(Book.class);
		 r.add(Section.class);
		 return r;
	}
}
