package my.home.logback.extension;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

/**
 * Default implementation of JsonFormatter with default setting.
 * 
 * @author Espinosa
 */
public class JacksonSerializer implements JsonSerializer {
	
	private static final ObjectMapper mapper;
	static {
		mapper = new ObjectMapper();
		mapper.disable(SerializationConfig.Feature.WRITE_DATE_KEYS_AS_TIMESTAMPS); // use ISO date format, example: "2016-10-02"
		// register modules here
	}

	@Override
	public String serialize(Object o) {
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
		} catch (IOException e) {
			return "Formating to JSON failed : " + o.toString();
		}
	}
}
