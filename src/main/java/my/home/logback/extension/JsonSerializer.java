package my.home.logback.extension;

public interface JsonSerializer {
	String serialize(Object o);
}
