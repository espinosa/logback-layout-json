package my.home.logback.extension;

import java.util.Map;

import org.slf4j.Marker;
import org.slf4j.helpers.MessageFormatter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.LoggerContextVO;

public class ModifiedLoggingEvent implements ILoggingEvent {
	private final ILoggingEvent originalEvent;
	private final Object[] argumentArray;
	private String formattedMessage;
	
	public ModifiedLoggingEvent(ILoggingEvent event, Object[] argumentArray) {
		this.originalEvent = event;
		this.argumentArray = argumentArray;
	}
	
	@Override
	public String getThreadName() {
		return originalEvent.getThreadName();
	}
	
	@Override
	public Level getLevel() {
		return originalEvent.getLevel();
	}
	
	@Override
	public String getMessage() {
		return originalEvent.getMessage();
	}
	
	@Override
	public Object[] getArgumentArray() {
		return originalEvent.getArgumentArray();
	}
	
	@Override
	public String getFormattedMessage() {
		if (argumentArray != null) {
			formattedMessage = MessageFormatter.arrayFormat(getMessage(), argumentArray).getMessage();
		} else {
			formattedMessage = getMessage();
		}
		return formattedMessage;
	}
	
	@Override
	public String getLoggerName() {
		return originalEvent.getLoggerName();
	}
	
	@Override
	public LoggerContextVO getLoggerContextVO() {
		return originalEvent.getLoggerContextVO();
	}
	
	@Override
	public IThrowableProxy getThrowableProxy() {
		return originalEvent.getThrowableProxy();
	}
	
	@Override
	public StackTraceElement[] getCallerData() {
		return originalEvent.getCallerData();
	}
	
	@Override
	public boolean hasCallerData() {
		return originalEvent.hasCallerData();
	}
	
	@Override
	public Marker getMarker() {
		return originalEvent.getMarker();
	}
	
	@Override
	public Map<String, String> getMDCPropertyMap() {
		return originalEvent.getMDCPropertyMap();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public Map<String, String> getMdc() {
		return originalEvent.getMdc();
	}
	
	@Override
	public long getTimeStamp() {
		return originalEvent.getTimeStamp();
	}
	
	@Override
	public void prepareForDeferredProcessing() {
		originalEvent.prepareForDeferredProcessing();
	}
}
