package my.home.logback.extension;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;

public class JsonPrintPatternLayout extends PatternLayout {
	private JsonPrintPatternLayoutConfig config = null;
	private JsonSerializer jsonSerializer = null;

	@Override
	public void start() {
		super.start();
		if (config == null) {
			config = new TestConfig();
		}
		jsonSerializer = config.createSerializer();
	}

	@Override
	public String doLayout(ILoggingEvent event) {
		Object[] beans = extractBeans(event.getArgumentArray());
		boolean atLeastOneToBeSerialized = false;
		for (int i = 0; i < beans.length; i++) {
			Object bean = beans[i];
			if (isAllowedForSerialization(event, bean)) {
				beans[i] = jsonSerializer.serialize(bean);
				atLeastOneToBeSerialized = true;
			}
		}
		if (atLeastOneToBeSerialized) {
			return super.doLayout(new ModifiedLoggingEvent(event, beans));
		} else {
			return super.doLayout(event);
		}
	}

	/**
	 * Layout configuration option. To be called externally.
	 * If config is not specified then default implementation is used.
	 * 
	 * @param configClass
	 * @throws ClassNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	public void setConfig(String configClassName) 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {		
		Class<JsonPrintPatternLayoutConfig> configClass = (Class<JsonPrintPatternLayoutConfig>) Class.forName(configClassName);
		config = configClass.newInstance();
	}

	/**
	 * Skip messages with no arguments or ar
	 * @param event
	 * @param bean
	 * @return
	 */
	private boolean isAllowedForSerialization(ILoggingEvent event, Object bean) {
		if (event.getArgumentArray() == null || event.getArgumentArray().length < 1) return false;
		if (bean == null) return false;
		return config.isAllowed(bean);
	}

	private Object[] extractBeans(Object[] eventArgumentArray) {
		if (eventArgumentArray != null) {
			return eventArgumentArray;
		} else {
			return new Object[]{};
		}
	}
}
