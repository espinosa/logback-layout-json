package my.home.logback.extension;

import java.util.Set;

public abstract class ClassBasedConfig implements JsonPrintPatternLayoutConfig {
	
	private final Set<Class<?>> classWhitelist;
	
	/**
	 * Define allowed classes, define whitelist.
	 */
	abstract Set<Class<?>> allowedClasses();
	
	public ClassBasedConfig() {
		classWhitelist = allowedClasses();
	}
	
	@Override
	public JsonSerializer createSerializer() {
		return new JacksonSerializer();
	}

	@Override
	public boolean isAllowed(Object objectToBeSerialized) {
		return classWhitelist.contains(objectToBeSerialized.getClass());
	}
}
