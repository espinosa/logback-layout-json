package my.home.logback.extension;

public interface JsonPrintPatternLayoutConfig {
	
	/**
	 * Enables overriding of JSON serializer (renderrer). Factory
	 * method. Adding modules or etra configuration options, like formating of
	 * dates, etc.
	 * 
	 * @return fully initilized JSON serializer
	 */
	JsonSerializer createSerializer();
	
	/**
	 * Check if It can be on class by class base, whitelist, blacklist, or allow
	 * everything.
	 * 
	 * Beware, not all objects can be serialized to Json, like cyclic graphs, or
	 * convenient like large object graphs.
	 * 
	 * @param objectToBeSerialized
	 * 
	 * @return true if given object is going to be serialized to JSON
	 */
	boolean isAllowed(Object objectToBeSerialized);
}
