Modified Logback PrintPatternLayout using Jackson to serialize message argument
=================================================================================

Useful for logging complex structured objects where default toString method, which is typically one line, is not very helpful; in situations where multi line, indented text would be preffered.  

Also useful in situations when objects does not have defined toString() or they are of poor quality, like for library classes outside of project scope.

Link to Logback project (logging framework, popular successor of Log4J) - http://logback.qos.ch/

Link to Jackson project (popular JSON processing library) - http://wiki.fasterxml.com/JacksonHome or https://github.com/FasterXML/jackson

Licence: Apache 2.0
https://www.apache.org/licenses/LICENSE-2.0

Example:

	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;
	
	import sample.bean.book.Book;
	
	// define logger
	private static final Logger LOG = LoggerFactory.getLogger("logger1");
	
	// here goes the logging, depending on logger configuration, it may...
	LOG.info("Book {}", book);

Producing output like:

	22:12:36.161 [main] INFO  logger2 - Book {
	  "name" : "History of Britain",
	  "author" : "John O'Farrel",
	  "sections" : [ {
	    "name" : "Section1",
	    "description" : ....

Example `logback.xml` or `logback-test.xml` configuration (relevant fragment):

    <appender name="STDOUT-with-JSON" class="ch.qos.logback.core.ConsoleAppender">
		<encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">
			<layout class="my.home.logback.extension.JsonPrintPatternLayout">
				<pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
				<config>my.home.logback.extension.TestConfig</config>
			</layout>
		</encoder>
	</appender>

If config class is not specified a default implementation will be used.

Beware, not all objects can be serialized to Json, like cyclic graphs, or convenient like large object graphs.
Whitelist based is recommended, see `ClassBasedConfig.java`. Extend this class and define allowed classes.

## Troubleshooter

Default `<encoder>` cannot be used, it gives error:

	ERROR in ch.qos.logback.core.joran.util.PropertySetter@2f943d71 - Could not invoke method setLayout in class ch.qos.logback.classic.encoder.PatternLayoutEncoder 
	with parameter of type ... java.lang.reflect.InvocationTargetException
	...
	Caused by: java.lang.UnsupportedOperationException: one cannot set the layout of ch.qos.logback.classic.encoder.PatternLayoutEncoder


